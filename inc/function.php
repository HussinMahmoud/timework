<?php


function getelemntbyendtimenull ($time_sheetApi){
    
    $result = $time_sheetApi->getArrayOfElement("SELECT * FROM `timework`.`time_sheet` WHERE `time_end` =0 ;");
    
    if ($result == NULL){
        return NULL;
    }
    
    return $result[0] ;
    
}

function getTotalByMonth ($time_sheetApi){
    $result = $time_sheetApi->getArrayOfElement("SELECT SUM(`time_sheet`.`total`) AS `totatime` FROM `timework`.`time_sheet` WHERE MONTH(`time_sheet`.`created`) = MONTH(CURRENT_DATE);");
    
    if ($result == NULL){
        return NULL;
    }
    $total = $result[0] ;
    return getFormat($total->totatime) ;
    
}
function getTotalByDay ($time_sheetApi){
    $result = $time_sheetApi->getArrayOfElement("SELECT SUM(`time_sheet`.`total`) AS `totatime` FROM `timework`.`time_sheet` WHERE DAY(`time_sheet`.`created`) = DAY(CURRENT_DATE);");
    
    if ($result == NULL) {
        return NULL;
    }

    $total = $result[0];
    return getFormat ($total->totatime);
}
//
function getTotalByWeek ($time_sheetApi){
    $result = $time_sheetApi->getArrayOfElement("SELECT SUM(`time_sheet`.`total`) AS `totatime` FROM `timework`.`time_sheet` WHERE WEEK(`time_sheet`.`created`) = WEEK(CURRENT_DATE);");
    
    if ($result == NULL) {
        return NULL;
    }

    $total = $result[0];
    return getFormat ($total->totatime);
}




function  getFormat ($total){
    $totalTime = (int)$total;
    $hours = intval($totalTime / 3600) ;
    $minutes = intval(($totalTime%3600)/60) ;
    $seconds = intval($totalTime%60) ;
    
    $result = "Hours:  ". $hours." Minutes:  ".$minutes." Seconds:  ".$seconds;
    
    return $result;

}

?>
