<?php



class ApiTabel {

    //put your code here
    private $myconnection;
    private $nameTabel;
    private $arrayColms;

    function __construct($nametabel, $arraycolms) {

        $this->nameTabel = $nametabel;
        $this->arrayColms = $arraycolms;
        $this->myconnection = Dbconnection::getmyconnection();
    }
  function tabel_get($extra = '') {
        // global $handle;
        $ex = strip_tags($extra);
        $nametabel = $this->nameTabel;

        $qurey = sprintf("SELECT * FROM `%s` %s  ;", $nametabel, $ex);
        $qurey_result = @mysql_query($qurey);
       

        if (!$qurey_result)
            return NULL;


        $rcount = @mysql_num_rows($qurey_result);

        if ($rcount == 0)
            return NULL;

        $elements = array();
        for ($i = 0; $i < $rcount; $i++) {
            $u = @mysql_fetch_object($qurey_result);
            $elements[@count($elements)] = $u;
        }


        return $elements;
    }

    function get_by_id($id) {
        $_id = (int) $id;

        if ($_id == 0)
            return NULL;

        $result = $this->tabel_get('WHERE id= ' . $_id);

        if ($result == NULL)
            return NULL;

        $element = $result[0];
        return $element;
    }

    
    
    function getArrayOfElement($extra = '') {
        //run any order of sql
        $qurey = sprintf(" %s", $extra);
        $qurey_result = @mysql_query($qurey);


        if (!$qurey_result){

            return NULL;

        }
        $rcount = @mysql_num_rows($qurey_result);

        if ($rcount == 0)
            return NULL;

        $elements = array();
        for ($i = 0; $i < $rcount; $i++) {
            $u = @mysql_fetch_object($qurey_result);
            $elements[@count($elements)] = $u;
        }


        return $elements;
    }
    
     function add_new_row($arrayofcolmsInsert) {

        $handle = $this->myconnection->gethandle();



        $testempty = 0;


        for ($i = 0; $i < count($arrayofcolmsInsert); $i++) {
            if (empty($arrayofcolmsInsert[$i])) {

                $testempty++;
            }
        }

        if ($testempty == count($arrayofcolmsInsert)) {
//            echo 'erro 1';
            return FALSE;
        }

        for ($i = 0; $i < count($this->arrayColms) - 2; $i++) { /* i give 2 to created and modfid time&date to insert that alraedy   */
            switch ($this->arrayColms[$i][1]):
                case 'str':
                    $arrayofcolmsInsert[$i] = @mysql_real_escape_string(strip_tags($arrayofcolmsInsert[$i]), $handle);
//                    echo $arrayofcolmsInsert[$i].'<br/>';
                    break;

                case 'int':
                    $arrayofcolmsInsert[$i] = (int) $arrayofcolmsInsert[$i];
                    break;

                case 'pass':
                    $arrayofcolmsInsert[$i] = @md5(mysql_real_escape_string(strip_tags($arrayofcolmsInsert[$i]), $handle));
                    break;
                case 'html':
                    $arrayofcolmsInsert[$i] = @mysql_real_escape_string($arrayofcolmsInsert[$i], $handle);

                    break;

                case 'mail':
                    $arrayofcolmsInsert[$i] = @mysql_real_escape_string(strip_tags($arrayofcolmsInsert[$i]), $handle);
                    if (!filter_var($arrayofcolmsInsert[$i], FILTER_VALIDATE_EMAIL)) {
                        
                        return FALSE;
                    }
                    break;

                default:
                    break;
            // $arrayofcolms[$i]=  $arrayofcolms[$i] ;  /*   to test    */   

            endswitch;
        }

        $query = "INSERT INTO `$this->nameTabel` ( ";

        // VALUES(NULL , 
        for ($i = 0; $i < count($this->arrayColms); $i++) {
            $namecolom = $this->arrayColms[$i][0];
            if ($i == count($this->arrayColms) - 1) {
                $query .= '`' . $namecolom . '`' . ')';
            } else {
                $query .= '`' . $namecolom . '`' . ' , ';
            }
        }

        $query .= "VALUES( ";




        for ($i = 0; $i < count($arrayofcolmsInsert); $i++) {
            $valucolom = $arrayofcolmsInsert[$i];
            $query .= "'" . $valucolom . "'" . ' , ';
        }
        $timenow = date("Y-m-d H:i:s");
        $query .= "  '$timenow' , '$timenow' );";

        //  echo  $query ;
        $qresult = @mysql_query($query);

        if (!$qresult) {
//             echo 'erro 2';
//             echo '<br/>'.$query;
            return FALSE;
        }



        return TRUE;
    }
    
    function update_one_row($id, $timeuse) {
        $_id = (int) $id;

        if ($_id == 0)
            return FALSE;

        $selectRow = $this->get_by_id($_id);

        if (!$selectRow)
            return FALSE;

         $qurey = "UPDATE `$this->nameTabel` SET `timeused` = '$timeuse' WHERE  `id`= $_id ;";
        
         $qresult = mysql_query($qurey);   

        if (!$qresult)
            return FALSE;


        return TRUE;
        
    }

    
    function update_old_one_row($id, $arrayOfInputs) {
        $_id = (int) $id;

        if ($_id == 0)
            return FALSE;

        $selectRow = $this->get_by_id($_id);

        if (!$selectRow)
            return FALSE;


        $fildes = array();

        $numNoChange = 0;


        for ($i = 0; $i < count($arrayOfInputs); $i++) {
            $nameColom = $this->arrayColms[$i][0];
            $oldValue = $selectRow->$nameColom;
            $testchange = 0;
            switch ($this->arrayColms[$i][1]):
                case 'str':
                case 'pass':
                case 'mail':
                case 'html':
                    if ((strcmp($arrayOfInputs[$i], $oldValue) == 0)|| $arrayOfInputs[$i]== "" || $arrayOfInputs[$i]== NULL) {
                        $testchange++;
                    
                    
                   
                    }elseif (strcmp($this->arrayColms[$i][1], 'mail') == 0) {
                        
                        
                        if (!filter_var($arrayOfInputs[$i], FILTER_VALIDATE_EMAIL)) {
//                            echo "<br/>".'erro mail   '."<br/>".$arrayOfInputs[$i] ;
//                            echo "<br/>".$oldValue;
//                            echo "<br/>".$arrayOfInputs[$i] ;
//                            echo strcmp($arrayOfInputs[$i], $oldValue) ;
                            return FALSE;
                        }
                        
                        
                        
                    } else {
                        $arrayOfInputs[$i] = $this->switchType($this->arrayColms[$i][1], $arrayOfInputs[$i]);
                        $fildes[@count($fildes)] = "`$nameColom` = '$arrayOfInputs[$i]' ";   /*  add function to switch case */
                    }
                    break;


                case 'int':
                    if (($arrayOfInputs[$i] == $oldValue) || ($arrayOfInputs[$i]== -1 )) {
                        $testchange++;
                    } else {
                        $arrayOfInputs[$i] = $this->switchType($this->arrayColms[$i][1], $arrayOfInputs[$i]);
                        $fildes[@count($fildes)] = "`$nameColom` = '$arrayOfInputs[$i]' ";
                    }
                    break;

                default:
                    break;
            endswitch;

            if ($testchange != 0) {
                $numNoChange++;
                continue;
            }
        }

        if ($numNoChange == count($arrayOfInputs)) {
//            echo 'no chaeng';
            return FALSE;
        }
        $qurey = "UPDATE `$this->nameTabel` SET ";

        for ($i = 0; $i < count($fildes); $i++) {

            $qurey .= $fildes[$i] . ' , ';
        }
        $timenow = date("Y-m-d H:i:s");
        $qurey .= "`modifiled` = '$timenow' WHERE `id`= $_id ;";
#############################################################
        $qresult = mysql_query($qurey);    // want insert @

        if (!$qresult)
            return FALSE;


        return TRUE;
    }

    function switchType($typeInput, $input) {
        $handle = $this->myconnection->gethandle();
        switch ($typeInput):
            case 'str':
            case 'mail':
                $resulet = @mysql_real_escape_string(strip_tags($input), $handle);

                break;
             case 'html':
                    $resulet = @mysql_real_escape_string($input, $handle);

                    break;
            case 'int':
                $resulet = (int) $input;
                break;

            case 'pass':
                $resulet = @md5(mysql_real_escape_string(strip_tags($input), $handle));
                break;

            default:
                break;
        endswitch;
        return $resulet;
    }
    
    
    
}




?>
